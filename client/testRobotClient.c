#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdint.h>
#include <netdb.h> 
#include <string.h> 

#define SERV_ADDR "0.0.0.0"     
#define SERV_PORT 1024			
#define TEAM_ID   1             /* Your team ID */

#define MSG_ACK    0
#define MSG_START  1
#define MSG_STOP   2
#define MSG_KICK   3
#define MSG_TEST   4
#define MSG_OBJ_ID 5

#define SA struct sockaddr 
#define Sleep( msec ) usleep(( msec ) * 1000 )


int s;
uint16_t msgId = 0;

// This reads from 0 to maxSize bits from server and it stores it in buffer
int read_from_server (int sock, uint8_t* buffer, size_t maxSize) {

  int bytes_read = read (sock, buffer, maxSize);

  if (bytes_read <= 0) {
    fprintf (stderr, "[ERR] Server unexpectedly closed connection...\n");
    close (s);
    exit (EXIT_FAILURE);
  }

  printf ("[DEBUG] Received %d bytes\n", bytes_read);
  return bytes_read;
}

void funct (int sockfd) {

	/*
		Send a TEST request
		Receive ACK
		
		Send Cylinder identification
		Receive ACK
		
		Receive STOP	
	*/
  printf ("[DEBUG] Ready to send a TEST message\n");
  uint8_t* buffer;
  buffer = (uint8_t*) malloc(8*sizeof(uint8_t));
  
  if (buffer==NULL) {
		printf("[ERR] Memory finished\n");
	}
  uint16_t idTestmsg = 29000; //id random 
  buffer[0] = (uint8_t) idTestmsg;
  buffer[1] = (uint8_t)(idTestmsg >> 8);
  buffer[2] = TEAM_ID; //src, us
  buffer[3] = 255;  //dst, server (id=255)
  buffer[4] = MSG_TEST; //Message type
  buffer[5] = 0;
  buffer[6] = 0;
  buffer[7] = 0; //3 random bytes
  write(sockfd, buffer, 8);
  
  printf ("[DEBUG] Ready to receive an ACK\n");
  read_from_server (sockfd, buffer, 8);
  //uint16_t id = buffer[0] | buffer[1] << 8; //i don't care about ack id
  uint8_t src = buffer[2];
  uint8_t dst = buffer[3];
  uint8_t type = buffer[4];
  uint8_t error = buffer[5];
  uint16_t idConfirmed = buffer[6] | buffer[7] << 8;
  
  if (src!=255) {
  		printf("[ERR] I expected a message from the server (id=255), but I received a message from a foreigner (id sender = %d)\n", src);
	    free(buffer);
		exit(65);
  }
  
  if (dst != TEAM_ID) {
		printf("[ERR] I expected a message addressed to me (id = %d), but I wasn't the receiver (id receiver = %d)\n", TEAM_ID, dst);
	    free(buffer);
		exit(75);	
  }
  
  if (type!=0) {
  		printf("[ERR] I expected an ACK (type=0), but I received another type of message\n");
	    free(buffer);
  		exit(55);
  }
  
  if (error>0) {
  		printf("[ERR] My last message was not well processed: %d\n", error);
	    free(buffer);
   		exit(85);
  }

  if (idConfirmed != idTestmsg) {
  		printf("[ERR] ID issue\n");
	    free(buffer);
   		exit(95);
  }
  
  printf("[DEBUG] ACK received\n");  

  //Send  "cylinder identified" at position (6, 12)
  printf ("[DEBUG] Ready to send a OBJ_ID message (cylinder at position (0, 0)\n");
  uint16_t idCylindermsg = 37854; //id  
  buffer[0] = (uint8_t) idCylindermsg;
  buffer[1] = (uint8_t)(idCylindermsg >> 8);
  buffer[2] = TEAM_ID; //src, us
  buffer[3] = 255;  //dst, server (id=255)
  buffer[4] = MSG_OBJ_ID; //Message type
  buffer[5] = 6; //cylinder
  buffer[6] = 0; //x
  buffer[7] = 0; //y
  write(sockfd, buffer, 8);
  
  
  printf ("[DEBUG] Ready to receive an ACK\n");
  
  read_from_server (sockfd, buffer, 8);
  //uint16_t id = buffer[0] | buffer[1] << 8; //i don't care about ack id
  src = buffer[2];
  dst = buffer[3];
  type = buffer[4];
  error = buffer[5];
  idConfirmed = buffer[6] | buffer[7] << 8;
  
  if (src!=255) {
  		printf("[ERR] I expected a message from the server (id=255), but I received a message from a foreigner (id sender = %d)\n", src);
	    free(buffer);
		exit(65);
  }
  
  if (dst != TEAM_ID) {
		printf("[ERR] I expected a message addressed to me (id = %d), but I wasn't the receiver (id receiver = %d)\n", TEAM_ID, dst);
	    free(buffer);
		exit(75);	
  }
  
  if (type!=0) {
  		printf("[ERR] I expected an ACK (type=0), but I received another type of message\n");
	    free(buffer);
  		exit(55);
  }
  
  if (error>0) {
  		printf("[ERR] My last message was not well processed: %d\n", error);
	    free(buffer);
   		exit(85);
  }

  if (idConfirmed != idCylindermsg) {
  		printf("[ERR] ID issue\n");
	    free(buffer);
   		exit(95);
  }
  
  printf("[DEBUG] ACK received\n"); 
  
  
  
  //////////////////////////////////////////////////////////////////////////////////////
  
  //Send  "cylinder identified" at position (6, 12)
  printf ("[DEBUG] Ready to send a correction OBJ_ID message (inversed 4s pyramid at position (60, 22)\n");
  idCylindermsg = 37889; //id  
  buffer[0] = (uint8_t) idCylindermsg;
  buffer[1] = (uint8_t)(idCylindermsg >> 8);
  buffer[2] = TEAM_ID; //src, us
  buffer[3] = 255;  //dst, server (id=255)
  buffer[4] = MSG_OBJ_ID; //Message type
  buffer[5] = 4; //cylinder
  buffer[6] = 60; //x
  buffer[7] = 22; //y
  write(sockfd, buffer, 8);
  
  
  printf ("[DEBUG] Ready to receive an ACK\n");
  
  read_from_server (sockfd, buffer, 8);
  
  
  //////////////////////////////////////////////////////////////////////////////////////
  
  
  
  printf ("[DEBUG] Ready to receive a STOP\n");

  read_from_server (sockfd, buffer, 8);
  //uint16_t id = buffer[0] | buffer[1] << 8; //i don't care about ack id
  src = buffer[2];
  dst = buffer[3];
  type = buffer[4];
  // the rest is random
    
  if (src!=255) {
  		printf("[ERR] I expected a message from the server (id=255), but I received a message from a foreigner (id sender = %d)\n", src);
	    free(buffer);
		exit(65);
  }
  
  if (dst != TEAM_ID) {
		printf("[ERR] I expected a message addressed to me (id = %d), but I wasn't the receiver (id receiver = %d)\n", TEAM_ID, dst);
	    free(buffer);
		exit(75);	
  }
  
  if (type!=2) {
  		printf("[ERR] I expected a STOP (type=2), but I received another type of message\n");
	    free(buffer);
  		exit(55);
  }
  
  printf("[DEBUG] STOP received\n"); 
  free(buffer);
  return;
}


int main(int argc, char **argv) {
	int sockfd, connfd; 
	uint8_t* buffer;
	struct sockaddr_in servaddr, cli; 

	// socket create and verification 
	sockfd = socket(AF_INET, SOCK_STREAM, 0); 
	if (sockfd == -1) { 
		printf("[ERR] Socket creation failed...\n"); 
		exit(0); 
	} 
	else
		printf("[DEBUG] Socket successfully created..\n"); 
	bzero(&servaddr, sizeof(servaddr)); 

	// assign IP, PORT 
	servaddr.sin_family = AF_INET; 
	servaddr.sin_addr.s_addr = inet_addr(SERV_ADDR); 
	servaddr.sin_port = htons(SERV_PORT); 

	// connect the client socket to server socket 
	if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) { 
		printf("[ERR] Connection with the server failed...\n"); 
		exit(0); 
	} 
	else
		printf("[DEBUG] Connected to the server..\n"); 
		
	
	//Receive the start message and print the various parameters
	buffer = (uint8_t*) malloc(8*sizeof(uint8_t));
	if (buffer==NULL) {
		printf("[ERR] Memory finished\n");
	}
	read_from_server (sockfd, buffer, 8);
	
	uint16_t id = buffer[0] | buffer[1] << 8;
	
	uint8_t src = buffer[2];
	uint8_t dst = buffer[3];
	uint8_t type = buffer[4];
	
	printf("[DEBUG] ID of the start: %d\n", id);
	printf("[DEBUG] From: %d\n", src);
	printf("[DEBUG] To: %d\n", dst);
	printf("[DEBUG] Type: %d\n", type);	
	
	if (type == 1) {
		printf("[DEBUG] START message received\n");	
	}
	
	else {
		printf("[ERR] I expected a START message, but another message arrived\n");
		exit(55);	
	}
	
	if (src != 255) {
		printf("[ERR] I expected a message from the server (id=255), but I received a message from a foreigner\n");
		exit(65);	
	}
	
	if (dst != TEAM_ID) {
		printf("[ERR] I expected a message addressed to me (id = %d), but I wasn't the receiver (id receiver = %d)\n", TEAM_ID, dst);
		exit(75);	
	}
	
	
	// Function: send a TEST request, receive ACK, send cyinder identification, receive ACK, receive STOP msg
	funct(sockfd); 

	// close the socket 
	close(sockfd); 
	free(buffer);
  return 0;
}

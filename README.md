### For EV3

Follow the steps from [http://www.ev3dev.org/docs/getting-started/](http://www.ev3dev.org/docs/getting-started/) to download the ev3dev kernel image and flash it on the SD card.

### For the server
## How to COMPILE the server
The server used for the competition has been coded using `QT` [correct pronunciation: "cute", not "cue tea"].  
`QT` is a cross-platform software development framework for Embedded and Desktop. You can download the open source version of QT from: https://www.qt.io/download.  
Server has been developed using `QT v5.4, Ubuntu 16.04 - 64 bit`. It will work with more recent version of Ubuntu though.  
With few modifications, the source code can be adapted for Windows user too. However, we suggest to use a GNU/Linux-like operating system. We used c++11 for the development, yet QT also has proprietary libraries.  
Once QT has been installed, open it. Then, open an existing project and select `"ServerOS.pro"`. Code is divided in 3 different folders: Headers, Sources, Forms. Forms folder contains the GUI (Graphical User Interface) of the server.  
mainwindow.cpp contains the behaviour associated to each graphical element, whereas server.cpp handles the communication part.  
You are not expected to modify the source code unless you need to do it.


## How to RUN the server
A team file "teams.txt" is necessary to successfully execute the server.    
Each line of the team file should be formatted as:  
`[NAME] [IP_ADDRESS]`  
where:   
`[NAME]` = your team name, alphanumeric only, no spaces  
`[IP_ADDRESS]` = the ip address of your robot.  
Please, don't put any spaces after `[IP_ADDRESS]` and don't put empty lines.  
  
Example of teams.txt:  
```
Test 10.42.0.177  
Team_B 127.0.0.1  
Team_C 127.0.0.2  
Team_D 127.0.0.3  
Team_E 127.0.0.4  
```
    
`IMPORTANT`: During your tests your team id depends by your position in the teamfile, starting from 1. During the competition, your team id will be the one present in the section "Groups" in the OS website.
Do not remove the Test line: it is the number 0 and it has to be present ALWAYS.  
    
`IMPORTANT - 2`: don't touch the file ".donttouchme.txt". If you remove it by error, just close the server and re-create it, empty.  
  
You can run the server providing as program argument the absolute path of the folder containing the team file.  
Example: `$ ./server /home/tobi/ServerOS` , without a final `"/"`. Be sure that `"teams.txt"` is contained in the same folder of `"res"` folder, to correctly display the background image and the logo of Télécom Paris.  
In `Qt`, you can set program arguments clicking on `"Projects"` (left-side panel), then in the subsection `"Build & Run"` click on `"Run"`, then add your string in `"Command line arguments"`.  
  
Once runned, it is possible to:  
* Set-up the right server address in your client (ex. `10.42.0.1`, port `1024`) and your proper team id
* Select the teams (max 2, min 1) joining to the game by checking the box (right-side)  
* Click on 'Select'. This will gives to the selected team the authorisation to play, else their future connections will be rejected. Once done, it is possible to establish the socket connection.  
* Click on 'Unselect' to reverte point 2.  
* 'Reset log' will clear the log interface.  
* 'Save on file' button will save the current log + scores on a file whose name is "Log"+Current_Timestamp  
* In order to correct a score, use the button "+" and "-" (even without checking the box right-side).  
   If the text is the right side is empty, the motivation will be "None". Else, you can enter the text you prefer to remember the motivation of the correction.  
* When an object has been identified, its position will be displayed on the map.  
* Click on 'end game' to send a STOP message to all the active teams.  
   

## Protocol
To communicate between each other, EV3 must comply with the folowing communication protocol.  
Each message consists of a header and a body.   
Note that all numbers, unless explicitly stated otherwise, are unsigned integers whose formats are little-endian.  
  
# Header 
The `header` is 5-byte long:  
```
   0      1      2      3      4  
+------+------+------+------+------+  
|     ID      | src  | dst  | type |  
+------+------+------+------+------+  
```
Fields description:  
  
* `ID` is a 2-byte number (uint16_t) identifying the message (kind of like a sequence number). It is used when acknowledging messages.  
* `src` is a 1-byte number (uint8_t) identifying the team who sent the message (it is unique for the whole contest).  
* `dst` is a 1-byte number (uint8_t) identifying the team who should receive the message (it is unique for the whole contest).  
* `type` is a 1-byte (uint8_t) code that identifies the kind of message that is sent.  
  
Server uses always the identification number 255 (\xFF), so please use it as a dst when you send messages to server.  
Possible values for `type` are:  
* `0` for `ACK` message  
* `1` for `START` message  
* `2` for `STOP` message  
* `3` for `KICK` message, the message that has to be sent when a ball is kicked  
* `4` for `TEST` message, to test the connection between client and server. This message is used in a minimum working example provided along with the server code.  
* `5+` for `OBJ_ID` message, the message that has to be sent upon the identification of an object.
* `IMPORTANT`: About the latter, when you think you identified an object, you can send an `OBJ_ID` in the interval [5, 65536]. If there are two different objects, you have to use different `OBJ_ID`. If you want to correct an object, use the same `OBJ_ID`. The last sent will be taken into account. Example: I identified a cylinder at position (x=0, y=0), then I realized I made a mistake and I correct it in a inversed 4-sides pyramid at position (x=60, y=22). In this case, I use the same `OBJ_ID`. The client provided as example does exactly this thing.  
  
## Body
# ACK
`ACK` messages are used to acknowledge the reception of messages. They are 8-byte long (uint8_t) and they are only sent by the server:  
```
       0  - 4            5         6-7
+-------+-------+-------+-------+-------+
|      Header       |  error  |  ID ack | 
+-------+-------+-------+-------+-------+
```
Fields description:  
* `ID` ack is the ID of the message that is acknowledged  
* `error` is a status code. 0 -> OK, 1 -> error.  
* Messages sent by the server should not be acknowledged.  

# START
`START` messages can only be used by the server. One is sent to each active team when the game starts.  
If the robot disconnects and reconnects during the game, another START message will be sent to it right after it connects to the server.  
They are 8-byte long:  
```
       0  - 4         5-6-7
+-------+-------+-------+-------+
|      Header   |  random bits  |
+-------+-------+-------+-------+
```
  
# STOP
`STOP` messages are sent by server to every robot when the game ends. Robots are assume to stop immediately when a `STOP` message is received.
They are 8-bytes long:  
```
       0  - 4         5-6-7
+-------+-------+-------+-------+
|      Header   |  random bits  |
+-------+-------+-------+-------+
```
  
# MSG_KICK
`MSG_KICK` messages can only be sent by the client each time a robot thinks it has sucessfully kicked a ball to the other side.  
```
       0  - 4       5       6       7
+-------+-------+-------+-------+-------+
|      Header   |   x   |   y   |random |
+-------+-------+-------+-------+-------+
```
  
# Fields description:
* `x`: estimated coordinate x of the kicked ball  
* `y`: estimated coordinate y of the kicked ball  
* `random` bit: whatever  

# MSG_OBJ_ID
`MSG_OBJ_ID` must be sent each time a robot thinks it has sucessfully identified an object.  You can give a unique identifier to each object, as explained before in the 
header description section.
```
       0  - 4       5       6       7
+-------+-------+-------+-------+-------+
|      Header   | type  |   x   |   y   |
+-------+-------+-------+-------+-------+
```
  
Fields description:  
* `type`: 
- 1 for cube  
- 2 for a 4-side pyramid 
- 3 for a 3-side pyramid 
- 4 for a 4-side pyramid upside-down
- 5 for a 3-side pyramid upside-down
- 6 for cylinder
* `x`: estimated coordinate x of the object
* `y`: estimated coordinate y of the object
 

# TEST
`TEST` message is sent to ask the server for a connection test and to reproduce a minimum working example.  
```
       0  - 4         5-6-7
+-------+-------+-------+-------+
|      Header   |  random bits  |
+-------+-------+-------+-------+
```
If it is sent, the interactions shall follow the following sequence:  
* Server to Robot (`S2R`): START  
* Robot to Server (`R2S`): TEST   
* `S2R`: ACK  
* `RTS`: MSG_OBJ_ID: cylinder IDENTIFIED at position (x=6, y=12)  
* `S2R`: ACK  
* `RTS`: MSG_OBJ_ID: CORRECTION! inversed 4S-pyramid IDENTIFIED at position (x=60, y=22)  
* `S2R`: ACK  
* `S2R`: STOP  
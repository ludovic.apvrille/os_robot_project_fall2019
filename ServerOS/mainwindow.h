#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QMessageBox>
#include <QFile>
#include <QCheckBox>
#include <QTextStream>
#include <QDateTime>
#include <QList>
#include <QScrollBar>
#include <QTime>
#include <QMutex>
#include "ui_mainwindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    void printLog(QString *string);
    int32_t getTeamA();
    void setTeamA(int value);
    int32_t getTeamB();
    void setTeamB(int value);
    QString currentTimestamp();
    QMap<QString, QString> getTeams();
    QMap<int, QString> getLabelMap();
    QMap<QString, int> getipidMap();
    QMap<int, QTextEdit*> getScoreMap();
    void drawMap(char mapParam[10][12], QTextBrowser *mapBrowser);
    char referenceMap[10][12];
    char emptyMap[10][12];
    char mapA[10][12];
    char mapB[10][12];
    QMap<int, QList<QPair<int, int>>> objectsIdentifiedA;
    QMap<int, QList<QPair<int, int>>> objectsIdentifiedB;
    void resetMap(char mapParam[10][12]);
    QMap<QString, QLabel*> getUiLabelMap();
    ~MainWindow();

private slots:
    void handlePlusButton();
    void handleMinusButton();
    void saveLogOnDisk();
    void clearLog();
    void handleSelectButton();
    void handleUnselectButton();
    void handleEndButton();

public:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H

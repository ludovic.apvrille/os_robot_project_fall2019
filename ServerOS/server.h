#ifndef SERVER_H
#define SERVER_H

#include <QDialog>
#include <QDebug>
#include <QString>
#include <QVector>
#include <QtCore>
#include <QTcpServer>
#include <QtNetwork>
#include <QApplication>
#include <QTextCodec>
#include "mainwindow.h"
#include <QFileSystemWatcher>

class Server : public QObject
{
    Q_OBJECT
public:
    explicit Server(MainWindow *mw, QObject *parent = 0);
    QByteArray generateID(QByteArray idMsg_ba);
    QByteArray generateRandomID();
    void sendAck(QTcpSocket *socket, QByteArray idMsg_ba, bool error);
    void sendStart(QTcpSocket *socket);
    void sendStop(QTcpSocket *socket);
    void objectRecognizedFunction(int team, int idobj, int x, int y, int typeObj);
    void ballKickedFunction(int id);
    void testFunction();
    void sendStopFromUi();
    void takeMWfromMain(MainWindow *mw);
    MainWindow *mw;

signals:
    void dataReceived(QByteArray);
    //void fileChanged(const QString & path);

private slots:
    void newConnection();
    void disconnected();
    void readyRead();
    void fileChanged(const QString & path);

private:
    QTcpServer *server;
    QFileSystemWatcher * watcher;
    QHash<QTcpSocket*, QByteArray*> buffers; //We need a buffer to store data until block has completely received
    QHash<QTcpSocket*, qint32*> sizes; //We need to store the size to verify if a block has received completely
};



/*
* Header: 5 bytes long
*    msg ID: 2 bytes
*    src ID: 1 byte
*    dst ID: 1 byte, unique in this case
*    type: 1 byte. 0 for ACK, 1 for Start, 2 for STOP,
*                  3 for ball kicked, 4 for obj identified
*/


/* ACK msgs: 8 bytes long
 * Header (5 bytes)
 * 0 for OK, 1 for error: 1 byte
 * ID msg to be ack: 2 bytes
 */

 /* Start msgs: 8 bytes long
  * Header (5 bytes)
  * Random: 3 bytes
 */

 /* Stop msgs: 8 bytes long
  * Header (5 bytes)
  * Random: 3 bytes
 */

 /* Ball kicked msgs: 8 bytes long
  * Header (5 bytes)
  * x position: 1 byte
  * y position: 1 byte
  * Random: 1 byte
 */

 /* Object found msgs: 8 bytes long
  * Header (5 bytes)
  * id of the object: 1 for cube, 2 for pyramid,
  *       3 for triangular pyramid, 4 for inv pyramid,
  *       5 for inv triang pyramid, 6 for cylinder, 7 for ball.
  * x position: 1 byte
  * y position: 1 byte
 */

#endif // SERVER_H

#include "server.h"
#include "mainwindow.h"

//static inline void sendAck(QTcpSocket *socket, QByteArray idMsg_ba, QByteArray src, QByteArray dst, bool error);
//static inline QByteArray generateID(QByteArray idMsg_ba);
QMap<QTcpSocket*, QString> socketIPMap; //Socket, IP
QMap<QTcpSocket*, QString> socketTeamsMap; //Socket, TeamName
int test=2;
bool testFlag=false;
QString sourceFolderPath;

Server::Server(MainWindow *mw, QObject *parent) : QObject(parent)
{
    //Generate a tcp server that listen on any address and port 1024.
    //Upon a new connection request, call the method "newConnection()"
    this->mw = mw;
    server = new QTcpServer(this);
    connect(server, SIGNAL(newConnection()), SLOT(newConnection()));
    auto x = server->listen(QHostAddress::Any, 1024);
    mw->printLog(new QString("Listening: "+QString::number(x)));
    sourceFolderPath = QCoreApplication::arguments().at(1);
    watcher = new QFileSystemWatcher(this);
    //connect(watcher, SIGNAL(Server::fileChanged(const QString &)), this, SLOT(Server::fileChanged(const QString &)));
    connect(watcher, SIGNAL(fileChanged(const QString &)), this, SLOT(fileChanged(const QString &)));
    watcher->addPath(sourceFolderPath+"/.donttouchme.txt");
}

//Upon a new connection: create a socket, take the IP address, take client id, check whether client id
//is authorised (then if it is equal to team A or team B, then if someone pressed the button connect/authorise
//for team id. If so, call the method readyread and associate the socket with both the team id and the team name
//in a map. Else, close the connection.
void Server::newConnection()
{
    this->mw->printLog(new QString("Entered in new connection"));
    while (server->hasPendingConnections())
    {
        mw->printLog(new QString("Entered in hasPendingConnections"));
        QTcpSocket *socket = server->nextPendingConnection();
        connect(socket, SIGNAL(readyRead()), SLOT(readyRead()));
        connect(socket, SIGNAL(disconnected()), SLOT(disconnected()));

        QByteArray *buffer = new QByteArray();
        qint32 *s = new qint32(0);
        buffers.insert(socket, buffer);
        sizes.insert(socket, s);

        //Take IP address:
        QString ipClient =socket->peerAddress().toString();
        if (ipClient.contains("::ffff:"))
            ipClient = ipClient.replace("::ffff:", "");
        mw->printLog(new QString("New client: " +ipClient));

        //from IP, to ID
        int idclient = mw->getipidMap()[ipClient];
        //Check if allowed
        if (idclient == mw->getTeamA() || idclient == mw->getTeamB()) {
            mw->printLog(new QString("IP allowed"));
            socketIPMap.insert(socket, ipClient);
            socketTeamsMap.insert(socket, mw->getLabelMap()[idclient]);
            mw->printLog(new QString("Start sent"));

            //Set Map
            if (idclient == mw->getTeamA())
                mw->drawMap(mw->referenceMap, mw->ui->mapA);
            else
                mw->drawMap(mw->referenceMap, mw->ui->mapB);

            sendStart(socket);
        }
        else {
            mw->printLog(new QString(ipClient.size()));
            mw->printLog(new QString("IP not allowed"));
            socket->close();
        }
    }
}


//Close the conenction. Possibly, free team A and team B variables whether the disconnected team was playing.
//Remove the socket from the maps, it is not useful anymore.
void Server::disconnected()
{
    QTcpSocket *socket = static_cast<QTcpSocket*>(sender());
    QByteArray *buffer = buffers.value(socket);
    qint32 *s = sizes.value(socket);
    QString teamIP = socket->peerAddress().toString();//socketIPMap[socket]; //ASSUMPTION: all the teams have a different IP address
    if (teamIP.contains("::ffff:"))
        teamIP = teamIP.replace("::ffff:", "");
    socket->deleteLater();
    delete buffer;
    delete s;
    QMap<int, QString> labelMap = mw->getLabelMap(); //

    //Take disconnected team from socket
    int idteam = -1;
    if (mw->getipidMap().contains(teamIP))
        idteam = mw->getipidMap()[teamIP];    

    if (idteam == mw->getTeamA()) {
        //qDebug()<< "Team a disconnected" <<endl;
        //Clear Map
        //mw->drawMap(mw->emptyMap, mw->ui->mapA);
        mw->setTeamA(255);
    }
    else if (idteam == mw->getTeamB()) {
        //Clear Map
        //mw->drawMap(mw->emptyMap, mw->ui->mapB);
        mw->setTeamB(255);
    }

    if (labelMap.contains(idteam))
        mw->printLog(new QString("Disconnetted from: " +labelMap[idteam]));
    else
        mw->printLog(new QString("Disconnetted from: " +teamIP));

    socketIPMap.remove(socket);
    socketTeamsMap.remove(socket);
}

//Read from socket (max 80 bytes, but all the communications has to be done over 8 bytes).
void Server::readyRead()
{
    QTcpSocket *socket = static_cast<QTcpSocket*>(sender());
    QByteArray *buffer = buffers.value(socket);

    while (socket->bytesAvailable() > 0)
    {
        //mw->printLog(new QString("New Data"));
        buffer->append(socket->readAll());

        QString toPrinttmp(socketTeamsMap[socket]);
        toPrinttmp.append(" sent data. ");
        QString *toPrint = new QString(toPrinttmp);
        //toPrint->append(*buffer);
        mw->printLog(toPrint);

        // Save header in some variables: id msg, src, dst, type.
        QByteArray idMsg_ba = buffer->mid(0, 2);
        QByteArray src_ba   = buffer->mid(2, 1);
        QByteArray dst_ba   = buffer->mid(3, 1);
        QByteArray type_ba  = buffer->mid(4, 1);

        //Convert the byte arrays.
        quint16 idMsg_nb = idMsg_ba[0] | idMsg_ba[1] << 8;
        QString idMsg(idMsg_nb);
        qDebug()<< idMsg_nb;

        quint8 src = src_ba[0];
        quint8 dst = dst_ba[0];
        quint8 type = type_ba[0];

        if (src != mw->getTeamA() && src != mw->getTeamB()) { //FOR PH0WN! Weak part, on purpose.
            mw->printLog(new QString("Team " + QString::number(src) +" tried to connect whereas teams "
                                     + QString::number(mw->getTeamA()) + " and " + QString::number(mw->getTeamB()) +" are playing"));
            socket->close();
        }

        qDebug() << "src: " << src << " dst: " << dst << " type: " << type;
        // Check src, dst e type.
        if (type>=3 && dst==255 && (src == mw->getTeamA() || src == mw->getTeamB())) {
            sendAck(socket, idMsg_ba, false);

            if (type>=5) {
                QByteArray idobj_ba = buffer->mid(5, 1);
                QByteArray xpos_ba = buffer->mid(6, 1);
                QByteArray ypos_ba = buffer->mid(7, 1);

                quint8 typeObj = idobj_ba[0];
                quint8 xpos = xpos_ba[0];
                quint8 ypos = ypos_ba[0];

                objectRecognizedFunction(src, type, xpos, ypos, typeObj);
            }

            else if (type==3) {
                ballKickedFunction(src);
            }

            else if (type==4) {
                testFunction();
            }
        }
        else {
            sendAck(socket, idMsg_ba, true);
        }

        if(testFlag) {
            test--;            
        }
        if (test==0) {            
            mw->printLog(new QString("Send a STOP through END GAME BUTTON"));
            //sendStop(socket);
        }
        buffer->clear();
    }
}


//Send ack to socket
void Server::sendAck(QTcpSocket *socket, QByteArray idMsg_ba, bool error) {
    // Generate ID
    QByteArray ackMsg = generateRandomID();

    //Src -> Servermw->printLog(new QString("TEST request received from " + teamname)); -> 255
    quint8 srcnb = 255;
    QByteArray src(1, 0); //One byte only
    src[0] = (quint8)(srcnb);
    ackMsg.append(src);

    //Dst -> from socket
    quint8 dstnb = mw->getipidMap()[socketIPMap[socket]];
    QByteArray dst(1, 0); //One byte only
    dst[0] = (quint8)(dstnb);
    ackMsg.append(dst);

    //Type -> 0 for ack
    quint8 typenb = 0;
    QByteArray type(1, 0); //One byte only
    type[0] = (quint8)(typenb);
    ackMsg.append(type);

    //Error flag
    quint8 errorFlag;
    if (error)
        errorFlag=1;
    else
        errorFlag=0;
    QByteArray errorBA(1, 0); //One byte only
    errorBA[0] = (quint8)(errorFlag);
    ackMsg.append(errorBA);

    //Id of the msg acked
    ackMsg.append(idMsg_ba);

    //Write it
    socket->write(ackMsg);
    //QString *toprint = new QString("Size of the ack: ");
    //toprint->append(QString::number(ackMsg.size()));
    //mw->printLog(toprint);
    return;
}

void Server::sendStart(QTcpSocket *socket) {

    //ID of the start message
    QByteArray rid = generateRandomID();
    QByteArray startMsg(rid);
    //qDebug()<<"Size tmp: " << startMsg.size();

    //Source: 255 (the server)
    quint8 srcnb = 255;
    QByteArray src(1, 0); //One byte only
    src[0] = (quint8)(srcnb);
    startMsg.append(src);
    //qDebug()<<"Size tmp: " << startMsg.size();

    //Take dst id from socket
    //QString teamIP = socketIPMap[socket];
    quint8 dstnb = mw->getipidMap()[socketIPMap[socket]];
    QByteArray dst(1, 0); //One byte only
    dst[0] = (quint8)(dstnb);
    startMsg.append(dst);
    //qDebug()<<"Size tmp: " << startMsg.size();

    // 1 for type
    quint8 typenb = 1;
    QByteArray type(1, 0); //One byte only
    type[0] = (quint8)(typenb);
    startMsg.append(type);
    //qDebug()<<"Size tmp: " << startMsg.size();


    //3 random bytes
    quint8 randomnb = 14;
    QByteArray random(3, 0); //One byte only
    random[0] = (quint8)(randomnb);
    random[1] = (quint8)(randomnb);
    random[2] = (quint8)(randomnb);
    startMsg.append(random);
    //qDebug()<<"Size tmp: " << startMsg.size();

    socket->write(startMsg);
    return;
}

void Server::sendStop(QTcpSocket *socket) {
    //ID of the stop message
    QByteArray rid = generateRandomID();
    QByteArray stopMsg(rid);
    //qDebug()<<"Size tmp: " << stopMsg.size();

    //Source: 255 (the server)
    quint8 srcnb = 255;
    QByteArray src(1, 0); //One byte only
    src[0] = (quint8)(srcnb);
    stopMsg.append(src);
    //qDebug()<<"Size tmp: " << stopMsg.size();

    //Take dst id from socket
    //QString teamIP = socketIPMap[socket];
    quint8 dstnb = mw->getipidMap()[socketIPMap[socket]];
    QByteArray dst(1, 0); //One byte only
    dst[0] = (quint8)(dstnb);
    stopMsg.append(dst);
    //qDebug()<<"Size tmp: " << stopMsg.size();

    // 2 for type
    quint8 typenb = 2;
    QByteArray type(1, 0); //One byte only
    type[0] = (quint8)(typenb);
    stopMsg.append(type);
    //qDebug()<<"Size tmp: " << stopMsg.size();


    //3 random bytes
    quint8 randomnb = 1;
    QByteArray random(3, 0); //One byte only
    random[0] = (quint8)(randomnb);
    random[1] = (quint8)(randomnb);
    random[2] = (quint8)(randomnb);
    stopMsg.append(random);
    //qDebug()<<"Size tmp: " << stopMsg.size();

    socket->write(stopMsg);
    return;
}

//Random id over 2 bytes
QByteArray Server::generateRandomID() {
    quint16 r = qrand() % 65535;
    //qDebug() << "r:" << r;
    // storing r in QByteArray (little endian)
    QByteArray qBytes(2, 0); // reserve space for two bytes explicitly
    qBytes[0] = (quint8)r;
    qBytes[1] = (quint8)(r >> 8);
    return qBytes;
}

//Take team number, update score, print motivation
void Server::objectRecognizedFunction(int team, int idobj, int x, int y, int typeObj) {
    // Update score
    QTextEdit* score = mw->getScoreMap()[team];
    int scoreInt = score->toPlainText().toInt();
    QString newScore;
    QString motivation;
    int matrixX = y/10-1;
    int matrixY = x/10-1;

    if (matrixX<=0)
        matrixX=1;

    if (matrixY<=0)
        matrixY=1;
    bool alreadyPresent = false;

    if (team == mw->getTeamA()) {
        if (mw->objectsIdentifiedA.contains(idobj)) {
            alreadyPresent = true;
            QList<QPair<int, int>> listOfCoordinates = mw->objectsIdentifiedA[idobj];

            QPair<int, int> firstPair = listOfCoordinates[0];
            QPair<int, int> secondPair = listOfCoordinates[1];
            QPair<int, int> thirdPair = listOfCoordinates[2];
            QPair<int, int> fourthPair = listOfCoordinates[3];
            mw->mapA[firstPair.first][firstPair.second] = 'x';
            mw->mapA[secondPair.first][secondPair.second] = 'x';
            mw->mapA[thirdPair.first][thirdPair.second] = 'x';
            mw->mapA[fourthPair.first][fourthPair.second] = 'x';

            mw->drawMap(mw->mapA, mw->ui->mapA);
        }
    }
    else {
        if (mw->objectsIdentifiedB.contains(idobj)) {
            alreadyPresent = true;
            QList<QPair<int, int>> listOfCoordinates = mw->objectsIdentifiedB[idobj];

            QPair<int, int> firstPair = listOfCoordinates[0];
            QPair<int, int> secondPair = listOfCoordinates[1];
            QPair<int, int> thirdPair = listOfCoordinates[2];
            QPair<int, int> fourthPair = listOfCoordinates[3];
            mw->mapB[firstPair.first][firstPair.second] = 'x';
            mw->mapB[secondPair.first][secondPair.second] = 'x';
            mw->mapB[thirdPair.first][thirdPair.second] = 'x';
            mw->mapB[fourthPair.first][fourthPair.second] = 'x';

            mw->drawMap(mw->mapB, mw->ui->mapB);
        }
    }


    if (typeObj == 1) {
        motivation = "CUBE";

        if (alreadyPresent==true) {
            mw->printLog(new QString("Correction of a previous insertion!"));
            newScore = QString::number(scoreInt);
        }
        else {
            newScore = QString::number(scoreInt+3);
        }

        if (matrixX>=12) {
            matrixX=11;
        }
        if (matrixY>=10) {
            matrixY=9;
        }

        if (team == mw->getTeamA()) {
            mw->mapA[matrixX][matrixY] = 'q';
            mw->mapA[matrixX][matrixY+1] = 'q';
            mw->mapA[matrixX+1][matrixY] = 'q';
            mw->mapA[matrixX+1][matrixY+1] = 'q';
            mw->drawMap(mw->mapA, mw->ui->mapA);

            if (alreadyPresent==false) {
                QPair<int, int> first; first.first=matrixX; first.second=matrixY;
                QPair<int, int> second; second.first=matrixX; second.second=matrixY+1;
                QPair<int, int> third; third.first=matrixX+1; third.second=matrixY;
                QPair<int, int> fourth; fourth.first=matrixX+1; fourth.second=matrixY+1;
                mw->objectsIdentifiedA[idobj].append(first);
                mw->objectsIdentifiedA[idobj].append(second);
                mw->objectsIdentifiedA[idobj].append(third);
                mw->objectsIdentifiedA[idobj].append(fourth);
            }
        }
        else {
            mw->mapB[matrixX][matrixY] = 'q';
            mw->mapB[matrixX][matrixY+1] = 'q';
            mw->mapB[matrixX+1][matrixY] = 'q';
            mw->mapB[matrixX+1][matrixY+1] = 'q';
            mw->drawMap(mw->mapB, mw->ui->mapB);

            if (alreadyPresent==false) {
                QPair<int, int> first; first.first=matrixX; first.second=matrixY;
                QPair<int, int> second; second.first=matrixX; second.second=matrixY+1;
                QPair<int, int> third; third.first=matrixX+1; third.second=matrixY;
                QPair<int, int> fourth; fourth.first=matrixX+1; fourth.second=matrixY+1;
                mw->objectsIdentifiedB[idobj].append(first);
                mw->objectsIdentifiedB[idobj].append(second);
                mw->objectsIdentifiedB[idobj].append(third);
                mw->objectsIdentifiedB[idobj].append(fourth);
            }
        }
    }
    else if (typeObj == 2) {
        motivation = "4S-PYRAMID";
        if (alreadyPresent==true) {
            mw->printLog(new QString("Correction of a previous insertion!"));
            newScore = QString::number(scoreInt);
        }
        else {
            newScore = QString::number(scoreInt+3);
        }

        if (matrixX>=12) {
            matrixX=11;
        }
        if (matrixY>=10) {
            matrixY=9;
        }

        if (matrixY<=0) {
            matrixY=1;
        }

        if (team == mw->getTeamA()) {
            mw->mapA[matrixX][matrixY] = 'p';
            mw->mapA[matrixX+1][matrixY] = 'p';
            mw->mapA[matrixX+1][matrixY+1] = 'p';
            mw->mapA[matrixX+1][matrixY-1] = 'p';
            mw->drawMap(mw->mapA, mw->ui->mapA);

            if (alreadyPresent==false) {
                QPair<int, int> first; first.first=matrixX; first.second=matrixY;
                QPair<int, int> second; second.first=matrixX+1; second.second=matrixY;
                QPair<int, int> third; third.first=matrixX+1; third.second=matrixY+1;
                QPair<int, int> fourth; fourth.first=matrixX+1; fourth.second=matrixY-1;
                mw->objectsIdentifiedA[idobj].append(first);
                mw->objectsIdentifiedA[idobj].append(second);
                mw->objectsIdentifiedA[idobj].append(third);
                mw->objectsIdentifiedA[idobj].append(fourth);
            }
        }
        else {
            mw->mapB[matrixX][matrixY] = 'p';
            mw->mapB[matrixX+1][matrixY+1] = 'p';
            mw->mapB[matrixX+1][matrixY] = 'p';
            mw->mapB[matrixX+1][matrixY-1] = 'p';
            mw->drawMap(mw->mapB, mw->ui->mapB);

            if (alreadyPresent==false) {
                QPair<int, int> first; first.first=matrixX; first.second=matrixY;
                QPair<int, int> second; second.first=matrixX+1; second.second=matrixY;
                QPair<int, int> third; third.first=matrixX+1; third.second=matrixY+1;
                QPair<int, int> fourth; fourth.first=matrixX+1; fourth.second=matrixY-1;
                mw->objectsIdentifiedB[idobj].append(first);
                mw->objectsIdentifiedB[idobj].append(second);
                mw->objectsIdentifiedB[idobj].append(third);
                mw->objectsIdentifiedB[idobj].append(fourth);
            }
        }
    }

    else if (typeObj == 3) {
        motivation = "3S-PYRAMID";
        if (alreadyPresent==true) {
            mw->printLog(new QString("Correction of a previous insertion!"));
            newScore = QString::number(scoreInt);
        }
        else {
            newScore = QString::number(scoreInt+3);
        }

        if (matrixX>=12) {
            matrixX=11;
        }
        if (matrixY>=10) {
            matrixY=9;
        }

        if (matrixY<=0) {
            matrixY=1;
        }

        if (team == mw->getTeamA()) {
            mw->mapA[matrixX][matrixY] = 'h';
            mw->mapA[matrixX+1][matrixY+1] = 'h';
            mw->mapA[matrixX+1][matrixY] = 'h';
            mw->mapA[matrixX+1][matrixY-1] = 'h';
            mw->drawMap(mw->mapA, mw->ui->mapA);

            if (alreadyPresent==false) {
                QPair<int, int> first; first.first=matrixX; first.second=matrixY;
                QPair<int, int> second; second.first=matrixX+1; second.second=matrixY;
                QPair<int, int> third; third.first=matrixX+1; third.second=matrixY+1;
                QPair<int, int> fourth; fourth.first=matrixX+1; fourth.second=matrixY-1;
                mw->objectsIdentifiedA[idobj].append(first);
                mw->objectsIdentifiedA[idobj].append(second);
                mw->objectsIdentifiedA[idobj].append(third);
                mw->objectsIdentifiedA[idobj].append(fourth);
            }
        }
        else {
            mw->mapB[matrixX][matrixY] = 'h';
            mw->mapB[matrixX+1][matrixY+1] = 'h';
            mw->mapB[matrixX+1][matrixY] = 'h';
            mw->mapB[matrixX+1][matrixY-1] = 'h';
            mw->drawMap(mw->mapB, mw->ui->mapB);

            if (alreadyPresent==false) {
                QPair<int, int> first; first.first=matrixX; first.second=matrixY;
                QPair<int, int> second; second.first=matrixX+1; second.second=matrixY;
                QPair<int, int> third; third.first=matrixX+1; third.second=matrixY+1;
                QPair<int, int> fourth; fourth.first=matrixX+1; fourth.second=matrixY-1;
                mw->objectsIdentifiedB[idobj].append(first);
                mw->objectsIdentifiedB[idobj].append(second);
                mw->objectsIdentifiedB[idobj].append(third);
                mw->objectsIdentifiedB[idobj].append(fourth);
            }
        }
    }
    else if (typeObj == 4) {
        motivation = "INVERSED 4S-PYRAMID";
        if (alreadyPresent==true) {
            mw->printLog(new QString("Correction of a previous insertion!"));
            newScore = QString::number(scoreInt);
        }
        else {
            newScore = QString::number(scoreInt+3);
        }

        if (matrixX>=12) {
            matrixX=11;
        }
        if (matrixY>=10) {
            matrixY=9;
        }

        if (matrixY<=0) {
            matrixY=1;
        }

        if (team == mw->getTeamA()) {
            mw->mapA[matrixX][matrixY] = 'P';
            mw->mapA[matrixX+1][matrixY+1] = 'P';
            mw->mapA[matrixX+1][matrixY] = 'P';
            mw->mapA[matrixX+1][matrixY-1] = 'P';
            mw->drawMap(mw->mapA, mw->ui->mapA);

            if (alreadyPresent==false) {
                QPair<int, int> first; first.first=matrixX; first.second=matrixY;
                QPair<int, int> second; second.first=matrixX+1; second.second=matrixY;
                QPair<int, int> third; third.first=matrixX+1; third.second=matrixY+1;
                QPair<int, int> fourth; fourth.first=matrixX+1; fourth.second=matrixY-1;
                mw->objectsIdentifiedA[idobj].append(first);
                mw->objectsIdentifiedA[idobj].append(second);
                mw->objectsIdentifiedA[idobj].append(third);
                mw->objectsIdentifiedA[idobj].append(fourth);
            }
        }
        else {
            mw->mapB[matrixX][matrixY] = 'P';
            mw->mapB[matrixX+1][matrixY+1] = 'P';
            mw->mapB[matrixX+1][matrixY] = 'P';
            mw->mapB[matrixX+1][matrixY-1] = 'P';
            mw->drawMap(mw->mapB, mw->ui->mapB);

            if (alreadyPresent==false) {
                QPair<int, int> first; first.first=matrixX; first.second=matrixY;
                QPair<int, int> second; second.first=matrixX+1; second.second=matrixY;
                QPair<int, int> third; third.first=matrixX+1; third.second=matrixY+1;
                QPair<int, int> fourth; fourth.first=matrixX+1; fourth.second=matrixY-1;
                mw->objectsIdentifiedB[idobj].append(first);
                mw->objectsIdentifiedB[idobj].append(second);
                mw->objectsIdentifiedB[idobj].append(third);
                mw->objectsIdentifiedB[idobj].append(fourth);
            }
        }
    }
    else if (typeObj == 5) {
        motivation = "INVERSED 3S-PYRAMID";
        if (alreadyPresent==true) {
            mw->printLog(new QString("Correction of a previous insertion!"));
            newScore = QString::number(scoreInt);
        }
        else {
            newScore = QString::number(scoreInt+3);
        }

        if (matrixX>=12) {
            matrixX=11;
        }
        if (matrixY>=10) {
            matrixY=9;
        }

        if (matrixY<=0) {
            matrixY=1;
        }

        if (team == mw->getTeamA()) {
            mw->mapA[matrixX][matrixY] = 'H';
            mw->mapA[matrixX+1][matrixY+1] = 'H';
            mw->mapA[matrixX+1][matrixY] = 'H';
            mw->mapA[matrixX+1][matrixY-1] = 'H';
            mw->drawMap(mw->mapA, mw->ui->mapA);

            if (alreadyPresent==false) {
                QPair<int, int> first; first.first=matrixX; first.second=matrixY;
                QPair<int, int> second; second.first=matrixX+1; second.second=matrixY;
                QPair<int, int> third; third.first=matrixX+1; third.second=matrixY+1;
                QPair<int, int> fourth; fourth.first=matrixX+1; fourth.second=matrixY-1;
                mw->objectsIdentifiedA[idobj].append(first);
                mw->objectsIdentifiedA[idobj].append(second);
                mw->objectsIdentifiedA[idobj].append(third);
                mw->objectsIdentifiedA[idobj].append(fourth);
            }
        }
        else {
            mw->mapB[matrixX][matrixY] = 'H';
            mw->mapB[matrixX+1][matrixY+1] = 'H';
            mw->mapB[matrixX+1][matrixY] = 'H';
            mw->mapB[matrixX+1][matrixY-1] = 'H';
            mw->drawMap(mw->mapB, mw->ui->mapB);

            if (alreadyPresent==false) {
                QPair<int, int> first; first.first=matrixX; first.second=matrixY;
                QPair<int, int> second; second.first=matrixX+1; second.second=matrixY;
                QPair<int, int> third; third.first=matrixX+1; third.second=matrixY+1;
                QPair<int, int> fourth; fourth.first=matrixX+1; fourth.second=matrixY-1;
                mw->objectsIdentifiedB[idobj].append(first);
                mw->objectsIdentifiedB[idobj].append(second);
                mw->objectsIdentifiedB[idobj].append(third);
                mw->objectsIdentifiedB[idobj].append(fourth);
            }
        }
    }
    else if (typeObj == 6) {
        motivation = "CYLINDER";
        if (alreadyPresent==true) {
            mw->printLog(new QString("Correction of a previous insertion!"));
            newScore = QString::number(scoreInt);
        }
        else {
            newScore = QString::number(scoreInt+3);
        }

        if (matrixX==12) {
            matrixX=11;
        }
        if (matrixY==10) {
            matrixY=9;
        }

        if (team == mw->getTeamA()) {
            mw->mapA[matrixX][matrixY] = 'c';
            mw->mapA[matrixX][matrixY+1] = 'c';
            mw->mapA[matrixX+1][matrixY] = 'c';
            mw->mapA[matrixX+1][matrixY+1] = 'c';
            mw->drawMap(mw->mapA, mw->ui->mapA);

            if (alreadyPresent==false) {
                QPair<int, int> first; first.first=matrixX; first.second=matrixY;
                QPair<int, int> second; second.first=matrixX; second.second=matrixY+1;
                QPair<int, int> third; third.first=matrixX+1; third.second=matrixY;
                QPair<int, int> fourth; fourth.first=matrixX+1; fourth.second=matrixY+1;
                mw->objectsIdentifiedA[idobj].append(first);
                mw->objectsIdentifiedA[idobj].append(second);
                mw->objectsIdentifiedA[idobj].append(third);
                mw->objectsIdentifiedA[idobj].append(fourth);
            }
        }
        else {
            mw->mapB[matrixX][matrixY] = 'c';
            mw->mapB[matrixX][matrixY+1] = 'c';
            mw->mapB[matrixX+1][matrixY] = 'c';
            mw->mapB[matrixX+1][matrixY+1] = 'c';
            mw->drawMap(mw->mapB, mw->ui->mapB);

            if (alreadyPresent==false) {
                QPair<int, int> first; first.first=matrixX; first.second=matrixY;
                QPair<int, int> second; second.first=matrixX; second.second=matrixY+1;
                QPair<int, int> third; third.first=matrixX+1; third.second=matrixY;
                QPair<int, int> fourth; fourth.first=matrixX+1; fourth.second=matrixY+1;
                mw->objectsIdentifiedB[idobj].append(first);
                mw->objectsIdentifiedB[idobj].append(second);
                mw->objectsIdentifiedB[idobj].append(third);
                mw->objectsIdentifiedB[idobj].append(fourth);
            }
        }
    }
    else {
        motivation = "None";
    }

    score->setText(newScore);
    int diff;
    if (alreadyPresent==true)
        diff=0;
    else
        diff=3;
    mw->printLog(new QString("Team "+QString::number(team) +": "+ QString::number(diff) +" point(s), because: " +motivation + " recognized at position: ("
                              +QString::number(x)+", "+QString::number(y)+")"));
}

//Take team number, update score, print motivation
void Server::ballKickedFunction(int team) {
    QTextEdit* score = mw->getScoreMap()[team];
    int scoreInt = score->toPlainText().toInt();
    QString newScore = QString::number(scoreInt+3);
    score->setText(newScore);
    mw->printLog(new QString("Team "+QString::number(team) +": + 3 point, because: ball kicked"));
    return;
}

//Send a stop msg after two iterations.
void Server::testFunction() {
    test=2;
    testFlag=true;
    return;
}

//Stop from ui
void Server::sendStopFromUi() {
    //Save on file
    QMapIterator<QTcpSocket*, QString> it(socketTeamsMap);
    mw->printLog(new QString("End game signal sent"));
    while (it.hasNext()) {
        it.next();
        mw->printLog(new QString("Stop sent to team " + it.value()));        
        sendStop(it.key());
    }
}


//file changed
void Server::fileChanged(const QString &path) {
    qDebug() <<path<<endl;
    sendStopFromUi();
}

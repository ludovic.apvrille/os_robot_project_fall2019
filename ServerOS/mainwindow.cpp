#include "mainwindow.h"
#include "server.h"

QMap<QString, QString> teams; //TeamName, IP
QList<QPair<QString, QString>> teamList;

QMap<int, QString> labelMap; //int ID, TeamName
QMap<QString, QLabel*> uiLabelMap;
QMap<QString, int> ipidMap; //IP, int ID
QMap<int, QTextEdit*> scoreMap;
QMap<int, QTextEdit*> commitMap;
QMap<QPushButton*, int> plusMap;
QMap<QPushButton*, int> minusMap;
QMap<int, QCheckBox*> checkMap;
QString sourceFolder;

int teama = 255, teamb = 255;

//This is the constructor of the GUI. It builds all the graphical elements and it associated a behaviour to each of them.
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(870, 680));
    sourceFolder = QCoreApplication::arguments().at(1);
    QString filename = sourceFolder+"/teams.txt";
    QString fileLogo = sourceFolder+"/res/tp.png";
    QString fileBackground = sourceFolder+"/res/background.jpg";

    //Set background image
    QPixmap bkgnd(fileBackground);
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Background, bkgnd);
    this->setPalette(palette);

    //Set logo Télécom Paris
    QImage imageLogo(fileLogo);
    ui->logoLabel->setPixmap(QPixmap::fromImage(imageLogo));
    ui->logoLabel->setBackgroundRole(QPalette::Base);
    ui->logoLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    ui->logoLabel->setScaledContents(true);
    ui->logoLabel->show();

    //Read input file.
    //Pay attention: if there is a format error, the GUI will not be built.
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox msgBox;
        msgBox.setText("Invalid file. Syntax of each line: [teamName] [IP Address]");
        msgBox.exec();
        exit(-5);
    }

    //This creates the right-side panel of the GUI.
    //For each team, it prints the team name and its temporary score, it generates the button "+" and "-",
    //it generates the TextEdit where it is possible to write the motivation of a
    //point changing, it generates the checkbox where it is possible selecting a team.
    //All of this in an horizontal layout.
    while (!file.atEnd()) {
        QByteArray line = file.readLine();
        QString lineString(line);
        QStringList splitFile = lineString.split(' ');
        QList<QString>::iterator itList;
        for (itList = splitFile.begin(); itList != splitFile.end(); ++itList) {
            QString key = *itList;
            itList++;
            QString value = *itList;
            value.remove(value.size()-1, value.size());
            QPair<QString, QString> tmpPair;
            tmpPair.first = key;
            tmpPair.second = value;
            teamList.append(tmpPair);
            teams.insert(key, value);
            //ui->logConsole->insertPlainText("Team added: " +key + " " +value+"\n");
            printLog(new QString("Team added: " +key + " " +value));
      }
    }

    QGridLayout *gridLayoutX = new QGridLayout;

    QString* toPrint = new QString("Team file size: ");
    toPrint->append(QString::number(teams.count()));
    printLog(toPrint);
    //ui->logConsole->insertPlainText(*toPrint+"\n");

    ui->parentLayout->addLayout(gridLayoutX, teams.size(), 6, 0);
    //QMapIterator<QString, QString> itMap(teams);
    QList<QPair<QString, QString>>::iterator scannerL;

    QString ritchTextStart("<html><head><meta name=\"qrichtext\" content=\"1\" /></head>"
                  "<body style=\" white-space: pre-wrap; font-family:Sans Serif; "
                  "font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">"
                  "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; font-weight:bold; "
                  "margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:9pt; color:Yellow\">");
    QString ritchTextEnd("</p>""</body></html>");

    int counter = 0;
    for (scannerL = teamList.begin(); scannerL != teamList.end(); scannerL++) {
        //Each graphical element is saved in a map. Thus, it is possible to know to which team corresponds each
        //"+", "-" buttons, the checkbox, the commit textedit and so on.        

        QLabel *label = new QLabel;
        QString ritchText(ritchTextStart+scannerL->first+ritchTextEnd);
        label->setText(ritchText);
        gridLayoutX->addWidget(label, counter, 0, 1, 1, 0);
        uiLabelMap.insert(scannerL->first, label);
        labelMap.insert(counter, scannerL->first);
        ipidMap.insert(scannerL->second, counter);

        QTextEdit *score = new QTextEdit("0", 0);
        score->setFixedWidth(28);
        score->setFixedHeight(28);
        gridLayoutX->addWidget(score, counter, 1, 1, 1, 0);
        scoreMap.insert(counter, score);

        QString pString("+");
        QString mString("-");
        QPushButton *plus  = new QPushButton(pString, this);
        plus->setFixedSize(30, 30);
        gridLayoutX->addWidget(plus, counter, 2, 1, 1, 0);
        plusMap.insert(plus, counter);
        connect(plus, SIGNAL (clicked()), this, SLOT(handlePlusButton()));

        QPushButton *minus = new QPushButton(mString, 0);
        minus->setFixedSize(30, 30);
        gridLayoutX->addWidget(minus, counter, 3, 1, 1, 0);
        minusMap.insert(minus, counter);
        connect(minus, SIGNAL (clicked()), this, SLOT(handleMinusButton()));

        QTextEdit *commit = new QTextEdit(" ", 0);
        commit->setFixedWidth(200);
        commit->setFixedHeight(28);
        commitMap.insert(counter, commit);
        gridLayoutX->addWidget(commit, counter, 4, 1, 1, 0);

        QCheckBox *cbox = new QCheckBox;
        gridLayoutX->addWidget(cbox, counter, 5, 1, 1, 0);
        checkMap.insert(counter, cbox);

        counter++;
    }

    //Associate a behaviour to each button.
    QPushButton* saveButton = ui->saveButton;
    connect(saveButton, SIGNAL (clicked()), this, SLOT(saveLogOnDisk()));
    QPushButton* clearButton = ui->resetButton;
    connect(clearButton, SIGNAL (clicked()), this, SLOT(clearLog()));
    QPushButton* selectButton = ui->selectButton;
    connect(selectButton, SIGNAL (clicked()), this, SLOT(handleSelectButton()));
    QPushButton* unselectButton = ui->unselectButton;
    connect(unselectButton, SIGNAL (clicked()), this, SLOT(handleUnselectButton()));
    QPushButton* endButton = ui->endButton;
    connect(endButton, SIGNAL (clicked()), this, SLOT(handleEndButton()));
    //QPushButton



    //Reference Map
    for (int i=0; i<10; i++) {
        for (int j=0; j<12; j++) {
            referenceMap[i][j] = 'x';
            emptyMap[i][j] = ' ';
        }
    }

    resetMap(mapA);
    resetMap(mapB);
}

//This function print a string in the log console, bottom side of the GUI
void MainWindow::printLog(QString *string) {
    ui->logConsole->insertPlainText(*string+"\n");
    QScrollBar *sb = ui->logConsole->verticalScrollBar();
    sb->setValue(sb->maximum());
}

//When a "+" button is pressed, first of all the team id, team name, associated to the button
//are retrieved. Then, the commit string is printed, and the score updated of a quantity: 1.
void MainWindow::handlePlusButton()
{
    auto button = qobject_cast<QPushButton *>(sender());
    int nb = plusMap[button];
    QString team = labelMap[nb];
    QTextEdit* commit = commitMap[nb];
    QString motivation;
    if (commit->toPlainText().size()==0)
        motivation = QString("None");
    else
        motivation = commit->toPlainText();
    printLog(new QString(team+" + 1 point, because: " +motivation));

    // Update score
    QTextEdit* score = scoreMap[nb];
    int scoreInt = score->toPlainText().toInt();
    QString newScore = QString::number(scoreInt+1);
    score->setText(newScore);
}

//When a "-" button is pressed, first of all the team id, team name, associated to the button
//are retrieved. Then, the commit string is printed, and the score updated of a quantity: -1.
void MainWindow::handleMinusButton()
{
    auto button = qobject_cast<QPushButton *>(sender());
    int nb = minusMap[button];
    QString team = labelMap[nb];
    QTextEdit* commit = commitMap[nb];
    QString motivation;
    if (commit->toPlainText().size()==0)
        motivation = QString("None");
    else
        motivation = commit->toPlainText();
    printLog(new QString(team+" - 1 point, because: " +motivation));

    // Update score
    QTextEdit* score = scoreMap[nb];
    int scoreInt = score->toPlainText().toInt();
    QString newScore = QString::number(scoreInt-1);
    score->setText(newScore);
}

//Clear the console log
void MainWindow::clearLog() {
    QTextEdit* log = ui->logConsole;
    log->setText("");
    return;
}

//It take a textual screenshot of the GUI (log, scores) and it prints it on a file
//whose name is: path+/log+/Data+TimeStamp.txt
void MainWindow::saveLogOnDisk() {
    //QString path(qApp->applicationDirPath());
    QString path(sourceFolder);
    QTextEdit* log = ui->logConsole;
    QString ts = currentTimestamp();
    QString filename = QString(path+"/log/Data"+ts+".txt");
    printLog(new QString("Console state written on file "+filename));
    QFile file(filename);

    //Put log
    if(file.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text)) {
       QTextStream stream(&file);
       stream << log->toPlainText() << endl;
       //Put scores
       //QMap<int, QTextEdit*> scoreMap;
       int maxnb = scoreMap.size();
       stream << "Scores: " << endl;

       for (int i=0; i<maxnb; i++) {
            //Get team name
            QString name = labelMap[i];

            //Get score
            QString score = scoreMap[i]->toPlainText();
            stream << name << ": " << score << " points." << endl;
       }
    }
    file.close();
    return;
}

//Generate the current timestamp expressed as the number of ms from 1970
QString MainWindow::currentTimestamp() {
    QDateTime setTime = QDateTime::fromString (QString("1970-07-18T14:15:09"), Qt::ISODate);
    QDateTime current = QDateTime::currentDateTime();
    uint msecs = setTime.time().msecsTo(current.time());
    return  QString::number(msecs);
}

//It authorises the connection of 1 or 2 teams, assigning them to "Team A" or "Team B", if possible.
void MainWindow::handleSelectButton() {
    QString ritchTextStartGreen("<html><head><meta name=\"qrichtext\" content=\"1\" /></head>"
                  "<body style=\" white-space: pre-wrap; font-family:Sans Serif; "
                  "font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">"
                  "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; font-weight:bold; "
                  "margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:9pt; color:#28ff0b\">");
    QString ritchTextStartLB("<html><head><meta name=\"qrichtext\" content=\"1\" /></head>"
                  "<body style=\" white-space: pre-wrap; font-family:Sans Serif; "
                  "font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">"
                  "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; font-weight:bold; "
                  "margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:9pt; color:#00fff7\">");
    QString ritchTextEnd("</p>""</body></html>");

    //Take all selected states
    QList<int> listChecked;

    for (int i=0; i<checkMap.size(); i++) {
        QCheckBox* cb = checkMap[i];
        if (cb->checkState()) {
            listChecked.append(i);
        }
    }

    QMessageBox msgBox;
    msgBox.setText("You shall select from 1 to 2 teams");

    if (listChecked.size()<=0) {
        msgBox.exec();
        return;
    }

    if (listChecked.size()>2) {
        msgBox.exec();
        return;
    }

    for (int i=0; i<listChecked.size(); i++) {
        int index = listChecked[i];
        printLog(new QString("Team "+labelMap[index]+" selected for connection"));
    }

    if (listChecked.size()==1) {
        //Assign ID to the first free between mapA and mapB, if any.
        //Else, error and do nothing.
        if (teama == 255) {
            int index = listChecked[0];

            if (index==teamb) {
                printLog(new QString("Team " + labelMap[index] + " was already assigned to Team B"));
            }

            else {
                QString name = labelMap[index];
                printLog(new QString("Team " + name + " assigned to Team A"));
                QLabel *tmpLabel = uiLabelMap[name];
                QString ritchText(ritchTextStartGreen+name+ritchTextEnd);
                tmpLabel->setText(ritchText);

                teama = index;
            }
        }
        else if (teamb==255) {
            int index = listChecked[0];

            if (index==teama) {
                printLog(new QString("Team " + labelMap[index] + " was already assigned to Team A"));
            }

            else {
                QString name = labelMap[index];
                printLog(new QString("Team " + name + " assigned to Team B"));
                QLabel *tmpLabel = uiLabelMap[name];
                QString ritchText(ritchTextStartLB+name+ritchTextEnd);
                tmpLabel->setText(ritchText);

                teamb = index;
            }
        }
        else {
            printLog(new QString("Team A and B already occupied"));
        }
    }

    else if (listChecked.size()==2) {
        //Assign them to both mapA and mapB, if both free.
        //Else, error and do nothing.
        int index0 = listChecked[0];
        int index1 = listChecked[1];

        if (teama==255 && teamb==255) {
            printLog(new QString("Team " + labelMap[index0] + " assigned to Team A"));
            teama = index0;

            printLog(new QString("Team " + labelMap[index1] + " assigned to Team B"));
            teamb = index1;


            QString nameA = labelMap[index0];
            QLabel *tmpLabelA = uiLabelMap[nameA];
            QString ritchTextA(ritchTextStartGreen+nameA+ritchTextEnd);
            tmpLabelA->setText(ritchTextA);

            QString nameB = labelMap[index1];
            QLabel *tmpLabelB = uiLabelMap[nameB];
            QString ritchTextB(ritchTextStartLB+nameB+ritchTextEnd);
            tmpLabelB->setText(ritchTextB);

        }

        else {
            if (teama!=255 && teamb!=255) { //both occupied
                    printLog(new QString("Team A and B both already occupied by teams " + labelMap[teama] + " and "
                                     + labelMap[teamb]));
            } //end both occupied

            else if (teama == 255 && teamb != 255) { //only one occupied
                printLog(new QString("Team B is already assigned. Please fix it and re-select"));
            }
            else if (teamb == 255 && teama != 255) { //only one occupied
                printLog(new QString("Team A is already assigned. Please fix it and re-select"));
            }
        }
    }
    else {
        printLog(new QString("No support for three or more teams"));
    }
    return;
}


void MainWindow::handleUnselectButton() {
    QString ritchTextStartY("<html><head><meta name=\"qrichtext\" content=\"1\" /></head>"
                  "<body style=\" white-space: pre-wrap; font-family:Sans Serif; "
                  "font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">"
                  "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; font-weight:bold; "
                  "margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:9pt; color:Yellow\">");
    QString ritchTextEnd("</p>""</body></html>");

    /*QString nameA = labelMap[index0];
    QLabel *tmpLabelA = uiLabelMap[nameA];
    QString ritchText(ritchTextStartGreen+nameA+ritchTextEnd);
    tmpLabelA->setText(ritchText);*/


    //Take all selected states
    QList<int> listChecked;

    for (int i=0; i<checkMap.size(); i++) {
        QCheckBox* cb = checkMap[i];
        if (cb->checkState()) {
            listChecked.append(i);
        }
    }

    QMessageBox msgBox;
    msgBox.setText("You shall select from 1 to 2 teams");

    if (listChecked.size()<=0) {
        msgBox.exec();
        return;
    }

    if (listChecked.size()>2) {
        msgBox.exec();
        return;
    }

    for (int i=0; i<listChecked.size(); i++) {
        int index = listChecked[i];
        printLog(new QString("Team "+labelMap[index]+" unselected for connection"));
        QString nameA = labelMap[index];
        QLabel *tmpLabelA = uiLabelMap[nameA];
        QString ritchText(ritchTextStartY+nameA+ritchTextEnd);
        tmpLabelA->setText(ritchText);
    }

    if (listChecked.size()==1) {
        //Assign ID to the first free between mapA and mapB, if any.
        //Else, error and do nothing.
        int index = listChecked[0];
        if (teama == index) {
            printLog(new QString("Team A -> free"));
            //drawMap(emptyMap, ui->mapA);
            teama = 255;
        }
        else if (teamb==index) {
            printLog(new QString("Team B -> free"));
            //drawMap(emptyMap, ui->mapB);
            teamb = 255;
        }
        else {
            printLog(new QString("Team A and B not concerned with selected team"));
        }
    }

    else if (listChecked.size()==2) {
        //Assign them to both mapA and mapB, if both free.
        //Else, error and do nothing.
        int index0 = listChecked[0];
        int index1 = listChecked[1];
        int x=0;

        if (teama==index0 || teama==index1) {
            printLog(new QString("Team A -> free"));
            drawMap(emptyMap, ui->mapA);
            teama = 255;
            x++;
        }

        if (teamb==index0|| teamb==index1) {
            printLog(new QString("Team B -> free"));
            drawMap(emptyMap, ui->mapB);
            teamb = 255;
            x++;
        }

        if (x==0) {
            printLog(new QString("Both teams A and B not concerned with selected teams"));
        }
        else if (x==1) {
            printLog(new QString("One between team A and B not concerned with selected teams"));
        }
    }

    else {
        printLog(new QString("No support for three or more teams"));
    }

    return;
}


void MainWindow::handleEndButton() {
    //Send a signal to Server.cpp
    //Send STOP msg to both team a or b
    //sendStopFromUi();
    //server->sendStopFromUi();
    QString ritchTextStartY("<html><head><meta name=\"qrichtext\" content=\"1\" /></head>"
                  "<body style=\" white-space: pre-wrap; font-family:Sans Serif; "
                  "font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">"
                  "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; font-weight:bold; "
                  "margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:9pt; color:Yellow\">");
    QString ritchTextEnd("</p>""</body></html>");

    //Title with teams
    QString team1, team2;

    if (teama!=255) {
        team1 = labelMap[teama];
        QLabel *tmpLabelA = uiLabelMap[team1];
        QString ritchText(ritchTextStartY+team1+ritchTextEnd);
        tmpLabelA->setText(ritchText);
    }
    else
        team1 = "Nobody";

    if (teamb!=255) {
        team2 = labelMap[teamb];
        QLabel *tmpLabelA = uiLabelMap[team2];
        QString ritchText(ritchTextStartY+team2+ritchTextEnd);
        tmpLabelA->setText(ritchText);
    }
    else
        team2 = "Nobody";

    //QString path(qApp->applicationDirPath());
    QString path(sourceFolder);
    QString filename(path);
    filename.append("/log/");
    filename.append(team1);
    filename.append("_VS_");
    filename.append(team2);
    filename.append(currentTimestamp());
    filename.append(".txt");
    printLog(new QString("Game registered on file: "+filename));
    QFile file(filename);

    //Put log
    if(file.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text)) {
        QTextStream stream(&file);
        stream << "Game" << endl;

        if (teama!=255) {
            stream << team1 << endl;
            QString score1 = scoreMap[teama]->toPlainText();
            stream << "Score: " << score1 << endl;
            stream << "Map team A " << endl;
            stream << ui->mapA->toPlainText() << endl;
        }

        if (teamb!=255) {
            stream << teamb << endl;
            QString score2 = scoreMap[teamb]->toPlainText();
            stream << "Score: " << score2 << endl;
            stream << "Map team B " << endl;
            stream << ui->mapB->toPlainText() << endl;
            //stream << "Map " << endl;
        }
    }
    file.close();

    //Touch a file and close it: it will cause the reaction of the watcher in server.cpp
    QString filename2(sourceFolder);
    filename2.append("/.donttouchme.txt");
    QFile file2(filename2);

    //Put log
    if(file2.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text)) {
        QTextStream stream(&file2);
        stream << "" << endl;
    }
    file2.close();

    ui->mapA->clear();
    ui->mapB->clear();
    resetMap(mapA);
    resetMap(mapB);
    drawMap(emptyMap, ui->mapB);
    drawMap(emptyMap, ui->mapA);
    objectsIdentifiedA.clear();
    objectsIdentifiedB.clear();
}

void MainWindow::drawMap(char mapParam[10][12], QTextBrowser *mapBrowser) {
    //QTextBrowser *mapABrowser = ui->mapA;
    mapBrowser->clear();
    char tmp[10][7*12];

    for (int row=0; row<10; row++) {
        //int offset = row*12;
        for (int col=0; col<12*7; col+=7) {        //a123456na123456n
            tmp[row][col+0] = mapParam[row][col/7];
            tmp[row][col+1] = ' ';
            tmp[row][col+2] = ' ';
            tmp[row][col+3] = ' ';
            tmp[row][col+4] = ' ';
            tmp[row][col+5] = ' ';
            tmp[row][col+6] = ' ';
        }
    }


    for (int i=0; i<10; i++) {
        for (int j=0; j<12*7; j++) {
            //ui->mapA->moveCursor (QTextCursor::End);
            mapBrowser->insertPlainText(QString(tmp[i][j]));

            //if (j%12!=0)
                mapBrowser->moveCursor (QTextCursor::End);
        }
        mapBrowser->append(QString(""));
    }
}

void MainWindow::resetMap(char mapParam[10][12]) {
    for (int i=0; i<10; i++) {
        for (int j=0; j<12; j++) {
            mapParam[i][j] = referenceMap[i][j];
        }
    }
}

//Getters, Setters, destructor, etc.
int32_t MainWindow::getTeamA() {
    return teama;
}

void MainWindow::setTeamA(int value) {
    teama = value;
}

int32_t MainWindow::getTeamB() {
    return teamb;
}

void MainWindow::setTeamB(int value) {
    teamb = value;
}

QMap<QString, QString> MainWindow::getTeams(){
    return teams;
}

QMap<QString, QLabel*> MainWindow::getUiLabelMap(){
    return uiLabelMap;
}

QMap<int, QString> MainWindow::getLabelMap(){
    return labelMap;
}

QMap<QString, int> MainWindow::getipidMap(){
    return ipidMap;
}

QMap<int, QTextEdit*> MainWindow::getScoreMap() {
    return scoreMap;
}

MainWindow::~MainWindow()
{
    delete ui;
}
